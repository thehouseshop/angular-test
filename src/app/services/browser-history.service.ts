import { Injectable } from '@angular/core'
import { Description, STORAGE_KEY } from '../misc/constants'
import { LogService } from './log.service'
import { LocalStorageService } from './local-storage.service'

@Injectable({
  providedIn: 'root',
})
export class BrowserHistoryService {
  forwardStack: Array<string>
  backwardStack: Array<string>
  currUrl = ''

  constructor(private storage: LocalStorageService, private log: LogService) {
    this.backwardStack = []
    this.forwardStack = []
  }

  visit(url: string): void {
    if (this.currUrl) {
      this.backwardStack.push(this.currUrl)
    }
    this.currUrl = url
    const historyItem = {
      url,
      date: Date.now(),
    }

    let browserHistory = this.storage.get(STORAGE_KEY)
    if (
      browserHistory &&
      Array.isArray(browserHistory) &&
      browserHistory.length > 0
    ) {
      if (browserHistory.length >= 20) {
        browserHistory.shift()
      }
      browserHistory.push(historyItem)
      this.storage.set(STORAGE_KEY, browserHistory)
    } else {
      this.storage.set(STORAGE_KEY, [historyItem])
    }

    this.log.add(Description.NEW_ADDRESS, historyItem)
    this.forwardStack.length = 0
  }

  back(steps: number): string {
    if (!this.backwardStack.length) {
      return ''
    }

    const historyItem = {
      url: this.currUrl,
      date: Date.now(),
    }

    this.log.add(Description.BACK_CLICK, historyItem)

    for (let i = 0; i < steps; i++) {
      this.forwardStack.push(this.currUrl)
      if (this.backwardStack.length > 0) {
        this.currUrl = this.backwardStack.pop()
      } else {
        return this.currUrl
      }
    }

    return this.currUrl
  }

  forward(steps: number): string {
    if (!this.forwardStack.length) {
      return ''
    }
    const historyItem = {
      url: this.currUrl,
      date: Date.now(),
    }

    this.log.add(Description.FORWARD_CLICK, historyItem)
    for (let i = 0; i < steps; i++) {
      this.backwardStack.push(this.currUrl)
      if (this.forwardStack.length > 0) {
        this.currUrl = this.forwardStack.pop()
      } else {
        return this.currUrl
      }
    }

    return this.currUrl
  }

  getCurrUrl() {
    return this.currUrl
  }

  isBackClickable(): boolean {
    return this.backwardStack.length > 0
  }

  isForwardClickable(): boolean {
    return this.forwardStack.length > 0
  }

  refresh() {
    this.visit(this.currUrl)
  }
}
