import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StackService {
  items: Array<string>;

  constructor() {
    this.items = []; 
   }

     // Push
  push(element: string){ 
      this.items.push(element); 
  } 
  // pop() 
  pop(){ 
    if (this.items.length == 0) 
        return -1; 
    return this.items.pop(); 
  } 
  // peek() 
  peek(){ 
    return this.items[this.items.length - 1]; 
  } 
  // isEmpty() 
  isEmpty(){ 
    return this.items.length == 0; 
  } 
}
