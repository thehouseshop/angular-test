import { TestBed } from '@angular/core/testing'

import { BrowserHistoryService } from './browser-history.service'
import { LocalStorageService } from './local-storage.service'
import { STORAGE_KEY } from '../misc/constants'

describe('BrowserHistoryService', () => {
  let service: BrowserHistoryService
  let storageServiceSpy: jasmine.SpyObj<LocalStorageService>;

  beforeEach(() => {
    const spy = jasmine.createSpyObj('LocalStorageService', ['get', 'set']);
    TestBed.configureTestingModule({
       providers: [
        BrowserHistoryService,
        { provide: LocalStorageService, useValue: spy }
      ]
    }
    )
    service = TestBed.inject(BrowserHistoryService)
    storageServiceSpy = TestBed.inject(LocalStorageService) as jasmine.SpyObj<LocalStorageService>;
  })

  it('should be created', () => {
    expect(service).toBeTruthy()
  })

  it('Should add new links', () => {
    service.visit('google.com')
    service.visit('yahoo.com')
    service.visit('bing.com')
    expect(service.getCurrUrl()).toBe('bing.com')
  })

  it('Should go backward once', () => {
    service.visit('google.com')
    service.visit('yahoo.com')
    service.visit('bing.com')
    expect(service.back(1)).toBe('yahoo.com')
    expect(service.getCurrUrl()).toBe('yahoo.com')
  })

  it('Should go forward once', () => {
    service.visit('google.com')
    service.visit('yahoo.com')
    service.visit('bing.com')
    expect(service.forward(1)).toBe('')
    expect(service.getCurrUrl()).toBe('bing.com')
  })

  it('Should go back once and forward by single step', () => {
    service.visit('google.com')
    service.visit('yahoo.com')
    service.visit('bing.com')
    expect(service.back(1)).toBe('yahoo.com')
    expect(service.getCurrUrl()).toBe('yahoo.com')
    expect(service.forward(1)).toBe('bing.com')
    expect(service.getCurrUrl()).toBe('bing.com')
  })

  it('Should go back by multiple steps and forward by multiple steps', () => {
    service.visit('google.com')
    service.visit('yahoo.com')
    service.visit('bing.com')
    service.visit('instagram.com')
    service.visit('abc.com')
    service.visit('test.com')
    expect(service.back(3)).toBe('bing.com')
    expect(service.getCurrUrl()).toBe('bing.com')
    expect(service.forward(2)).toBe('abc.com')
    expect(service.getCurrUrl()).toBe('abc.com')
  })

  it('Should go back once and forward by multiple steps', () => {
    service.visit('google.com')
    service.visit('yahoo.com')
    service.visit('bing.com')
    service.visit('instagram.com')
    service.visit('abc.com')
    service.visit('test.com')
    expect(service.back(1)).toBe('abc.com')
    expect(service.getCurrUrl()).toBe('abc.com')
    expect(service.forward(3)).toBe('test.com')
    expect(service.getCurrUrl()).toBe('test.com')
  })


  it('Should go forward once and back by multiple steps', () => {
    service.visit('google.com')
    service.visit('yahoo.com')
    service.visit('bing.com')
    service.visit('instagram.com')
    service.visit('abc.com')
    service.visit('test.com')
    expect(service.back(3)).toBe('bing.com')
    expect(service.getCurrUrl()).toBe('bing.com')
    expect(service.forward(1)).toBe('instagram.com')
    expect(service.getCurrUrl()).toBe('instagram.com')
  })

  it('Should go back more than available steps', () => {
    service.visit('google.com')
    service.visit('yahoo.com')
    service.visit('bing.com')
    service.visit('instagram.com')
    service.visit('abc.com')
    service.visit('test.com')
    expect(service.back(8)).toBe('google.com')
    expect(service.getCurrUrl()).toBe('google.com')
  })

  it('Should go forward more than available steps', () => {
    service.visit('google.com')
    service.visit('yahoo.com')
    service.visit('bing.com')
    service.visit('instagram.com')
    service.visit('abc.com')
    service.visit('test.com')
    expect(service.back(3)).toBe('bing.com')
    expect(service.getCurrUrl()).toBe('bing.com')
    expect(service.forward(6)).toBe('test.com')
    expect(service.getCurrUrl()).toBe('test.com')
  })


  it('should add url to empty array', () => {
    storageServiceSpy.get.and.returnValue([]);
    service.visit('google.com');
    const expectedValue = [{url: 'google.com', date: 123}]
    expect(storageServiceSpy.set).toHaveBeenCalledWith(STORAGE_KEY, [{url: 'google.com', date: jasmine.any(Number)}])
  })

  it('should add url to non empty array', () => {
    storageServiceSpy.get.and.returnValue([{url:'google.com', date: 123}, {url: 'bing.com', date: 123}]);
    service.visit('yahoo.com');
    const expectedValue = [
      {url:'google.com', date:jasmine.any(Number)}, 
      {url: 'bing.com', date: jasmine.any(Number)},
      {url: 'yahoo.com', date: jasmine.any(Number)}
    ]
    expect(storageServiceSpy.set).toHaveBeenCalledWith(STORAGE_KEY, expectedValue)
  })

  it('should add url to array.length >= 20', () => {
    storageServiceSpy.get.and.returnValue(
      [
        {url: 'google.com', date: 123},
        {url: 'bing.com', date: 123},
        {url: 'google.com', date: 123},
        {url: 'bing.com', date: 123},
        {url: 'google.com', date: 123},
        {url: 'bing.com', date: 123},
        {url: 'google.com', date: 123},
        {url: 'bing.com', date: 123},
        {url: 'google.com', date: 123},
        {url: 'bing.com', date: 123},
        {url: 'google.com', date: 123},
        {url: 'bing.com', date: 123},
        {url: 'google.com', date: 123},
        {url: 'bing.com', date: 123},
        {url: 'google.com', date: 123},
        {url: 'bing.com', date: 123},
        {url: 'google.com', date: 123},
        {url: 'bing.com', date: 123},
        {url: 'google.com', date: 123},
        {url: 'bing.com', date: 123}
       ]
    );
    service.visit('yahoo.com');

    const expectedValue =  [
      {url: 'bing.com', date: jasmine.any(Number)},
      {url: 'google.com', date: jasmine.any(Number)},
      {url: 'bing.com', date: jasmine.any(Number)},
      {url: 'google.com', date: jasmine.any(Number)},
      {url: 'bing.com', date: jasmine.any(Number)},
      {url: 'google.com', date: jasmine.any(Number)},
      {url: 'bing.com', date: jasmine.any(Number)},
      {url: 'google.com', date: jasmine.any(Number)},
      {url: 'bing.com', date: jasmine.any(Number)},
      {url: 'google.com', date: jasmine.any(Number)},
      {url: 'bing.com', date: jasmine.any(Number)},
      {url: 'google.com', date: jasmine.any(Number)},
      {url: 'bing.com', date: jasmine.any(Number)},
      {url: 'google.com', date: jasmine.any(Number)},
      {url: 'bing.com', date: jasmine.any(Number)},
      {url: 'google.com', date: jasmine.any(Number)},
      {url: 'bing.com', date: jasmine.any(Number)},
      {url: 'google.com', date: jasmine.any(Number)},
      {url: 'bing.com', date: jasmine.any(Number)},
      {url: 'yahoo.com', date: jasmine.any(Number)}
     ]
    expect(storageServiceSpy.set).toHaveBeenCalledWith(STORAGE_KEY, expectedValue)
  })
})
