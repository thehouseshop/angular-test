import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { Description, HistoryItem, LogItem } from '../misc/constants';

@Injectable({
  providedIn: 'root'
})
export class LogService {
  private items:LogItem[] = [];
  private $logs: BehaviorSubject<LogItem[]> = new BehaviorSubject(this.items);
 

  add(description: Description, historyItem: HistoryItem){
    this.items.push({
      date: historyItem.date,
      description,
      currentUrl: historyItem.url
    }
    )
    this.$logs.next(this.items);
  }


  get(): Observable<LogItem[]> {
    return this.$logs.asObservable();
  }  

  constructor() { }
}
