import { Injectable } from '@angular/core'
import { BehaviorSubject } from 'rxjs'
import { Tab } from '../models/tab.model'
import { BrowsingComponent } from '../views/browsing/browsing.component'
import { HistoryComponent } from '../views/history/history.component'
import { HOME_URL } from '../misc/constants'

@Injectable({
  providedIn: 'root',
})
export class TabService {
  constructor() {}

  tabs: Tab[] = [
    new Tab(BrowsingComponent, 'Browsing', { openedUrl: HOME_URL }),
    new Tab(HistoryComponent, 'History', {}),
  ]

  $tabs = new BehaviorSubject<Tab[]>(this.tabs)

  removeTab(index: number) {
    this.tabs.splice(index, 1)
    if (this.tabs.length > 0) {
      this.tabs[this.tabs.length - 1].active = true
    }
    this.$tabs.next(this.tabs)
  }

  addTab(tab: Tab) {
    for (let i = 0; i < this.tabs.length; i++) {
      if (this.tabs[i].active === true) {
        this.tabs[i].active = false
      }
    }
    tab.id = this.tabs.length + 1
    tab.active = true
    this.tabs.push(tab)
    this.$tabs.next(this.tabs)
  }
}
