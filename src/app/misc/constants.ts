export const HOME_URL = 'https://www.thehouseshop.com/';
export const STORAGE_KEY = 'app_browserHistory';

export enum Description {
    BACK_CLICK = "Button back clicked",
    FORWARD_CLICK = "Button forward clicked",
    NEW_ADDRESS = "New address entered",
    HOME_OPEN = "Home page opened"
} 

export interface LogItem {
    date: number;
    description: string;
    currentUrl: string;
}

export interface HistoryItem {
    date: number;
    url: string;
}

export interface ExampleComponent {
    data: any;
}