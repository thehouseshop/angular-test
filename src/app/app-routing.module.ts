import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrowserComponent } from './views/browser/browser.component';
import { BrowsingComponent } from './views/browsing/browsing.component';
import { HistoryComponent } from './views/history/history.component';

const routes: Routes = [
  {
    path: 'browser',
    component: BrowserComponent
  },
  {
    path: '',
    redirectTo: 'browser',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
