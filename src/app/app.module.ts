import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { BrowserComponent } from './views/browser/browser.component';
import { LogComponent } from './views/log/log.component';
import { TabsComponent } from './views/tabs/tabs.component';
import { ControlsPanelComponent } from './views/controls-panel/controls-panel.component';
import { UrlBarComponent } from './views/url-bar/url-bar.component';

import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { BrowsingComponent } from './views/browsing/browsing.component';
import { HistoryComponent } from './views/history/history.component';
import { TabContentComponent } from './common/tab-content/tab-content.component';
import { ExampleComponentComponent } from './common/example-component/example-component.component';
import { SafePipe } from './pipes/safe.pipe';

@NgModule({
  declarations: [
    AppComponent,
    BrowserComponent,
    LogComponent,
    TabsComponent,
    ControlsPanelComponent,
    UrlBarComponent,
    BrowsingComponent,
    HistoryComponent,
    TabContentComponent,
    ExampleComponentComponent,
    SafePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
  ],
  providers: [{ provide: 'WINDOW', useValue: window }],
  bootstrap: [AppComponent],
  entryComponents: [BrowsingComponent, HistoryComponent],
  
})
export class AppModule { }
