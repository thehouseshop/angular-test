import {
  Component,
  Input,
  ComponentFactoryResolver,
  ViewChild,
  OnInit,
  AfterViewInit,
  ViewContainerRef,
  ChangeDetectorRef
} from "@angular/core";
import { Tab } from "../../models/tab.model";
import { ExampleComponent } from '../../misc/constants';

@Component({
  selector: 'app-tab-content',
  templateUrl: './tab-content.component.html',
  styleUrls: ['./tab-content.component.scss']
})
export class TabContentComponent implements AfterViewInit {
  @Input() tab: Tab;
  @ViewChild('contentContainer', { read: ViewContainerRef }) container: ViewContainerRef;

  constructor(private componentFactoryResolver: ComponentFactoryResolver, private cdRef:ChangeDetectorRef) { }

  ngAfterViewInit(): void {
    const tab: Tab = this.tab;
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
      tab.component
    );
    const componentRef = this.container.createComponent(componentFactory);
    (componentRef.instance as ExampleComponent).data = tab.content;

    this.cdRef.detectChanges();
  }

}
