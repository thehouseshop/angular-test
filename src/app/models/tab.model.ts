import { Type } from '@angular/core';

export class Tab {
  public id: number;
  public label: string;
  public content: any;
  public active: boolean;
  public component: Type<any>;

  constructor(component: Type<any>, label: string, content: any) {
    this.content = content;
    this.component = component;
    this.label = label;
  }
}