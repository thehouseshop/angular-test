import { Component, OnInit } from '@angular/core'
import { Subscription } from 'rxjs'
import { LogService } from 'src/app/services/log.service'
import { LogItem } from '../../misc/constants'

@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.scss'],
})
export class LogComponent implements OnInit {
  logItems: LogItem[]
  subscription: Subscription

  constructor(private log: LogService) {}

  ngOnInit(): void {
    this.subscription = this.log.get().subscribe((res: LogItem[]) => {
      this.logItems = res
    })
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe()
      this.subscription = null
    }
  }
}
