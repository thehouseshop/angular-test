import { Component, OnInit } from '@angular/core';
import { HOME_URL } from 'src/app/misc/constants';
import { BrowserHistoryService } from 'src/app/services/browser-history.service';
import { LogService } from 'src/app/services/log.service';

@Component({
  selector: 'app-controls-panel',
  templateUrl: './controls-panel.component.html',
  styleUrls: ['./controls-panel.component.scss']
})
export class ControlsPanelComponent implements OnInit {

  constructor(private browserHistory: BrowserHistoryService) { }

  ngOnInit(): void {
  }

  onBackClick(){
     this.browserHistory.back(1);
  }

  onForwardClick(){
    this.browserHistory.forward(1);
  }

  onHomeClick(){
    this.browserHistory.visit(HOME_URL);
  }

  onRefreshClick(){
    this.browserHistory.refresh();
  }

  isBackClickable(): boolean{
   return this.browserHistory.isBackClickable();
  }

  isForwardClickable(): boolean{
    return this.browserHistory.isForwardClickable();
  }

}
