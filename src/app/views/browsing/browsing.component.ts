import { Component, Input, OnInit } from '@angular/core';
import { BrowserHistoryService } from 'src/app/services/browser-history.service';

@Component({
  selector: 'app-browsing',
  templateUrl: './browsing.component.html',
  styleUrls: ['./browsing.component.scss'],
  providers: [BrowserHistoryService]
})
export class BrowsingComponent implements OnInit {

  @Input() data: any;

  constructor(private browserHistory: BrowserHistoryService) { }

  ngOnInit(): void {
    if(this.data.openedUrl){
      this.browserHistory.visit(this.data.openedUrl);
    }
    
  }

  get currentUrl(): string {
    return this.browserHistory.getCurrUrl();
  }

}
