import { Component, Input, OnInit } from '@angular/core'
import { BrowserHistoryService } from 'src/app/services/browser-history.service'
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'

@Component({
  selector: 'app-url-bar',
  templateUrl: './url-bar.component.html',
  styleUrls: ['./url-bar.component.scss'],
})
export class UrlBarComponent implements OnInit {
  constructor(
    private browserHistory: BrowserHistoryService,
    private fb: FormBuilder,
  ) {}
  @Input() defaultUrl: string
  url: string
  urlPattern = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?'

  ngOnInit(): void {
    this.url = this.defaultUrl || this.currentUrl
  }

  onEnter(isUrlValid: boolean) {
    if (isUrlValid) {
      this.browserHistory.visit(this.url)
    }
  }

  get currentUrl() {
    return this.browserHistory.getCurrUrl()
  }
}
