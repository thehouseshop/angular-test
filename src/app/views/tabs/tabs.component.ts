import { Component, OnInit } from '@angular/core'
import { MatTabChangeEvent } from '@angular/material/tabs'
import { Tab } from '../../models/tab.model'
import { TabService } from 'src/app/services/tab.service'
import { BrowsingComponent } from '../browsing/browsing.component'
import { HistoryComponent } from '../history/history.component'

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss'],
})
export class TabsComponent implements OnInit {
  tabs = new Array<Tab>()
  selectedTab: number
  constructor(private tabService: TabService) {}

  ngOnInit(): void {
    this.tabService.$tabs.subscribe((tabs) => {
      this.tabs = tabs
      this.selectedTab = tabs.findIndex((tab) => tab.active)
    })
  }

  tabChanged(event: MatTabChangeEvent) {
    console.log('tab changed')
  }

  addNewBrowsingTab() {
    this.tabService.addTab(new Tab(BrowsingComponent, 'Browsing', {}))
  }

  addNewHistoryTab() {
    this.tabService.addTab(new Tab(HistoryComponent, 'History', {}))
  }

  removeTab(index: number): void {
    this.tabService.removeTab(index)
  }
}
