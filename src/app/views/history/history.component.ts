import { Component, OnInit, Input } from '@angular/core';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { HistoryItem, STORAGE_KEY } from '../../misc/constants';
import { BrowserHistoryService } from 'src/app/services/browser-history.service';
import { map } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
  providers: [BrowserHistoryService]
})
export class HistoryComponent implements OnInit {
  @Input() data: any;

  displayedColumns: string[] = ['date', 'url'];
  dataSource: Array<HistoryItem>;
  subscription: Subscription;
 
  constructor(private storage: LocalStorageService) { }

  ngOnInit(): void {
    this.subscription = this.storage.watch(STORAGE_KEY).subscribe((res: Array<HistoryItem>) => {
      const resCopy = [...res];
     this.dataSource = resCopy.reverse();
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
      this.subscription = null;
    }
  }

}
